package com.geektechnique.corefundamentals.challenge.controller;

import com.geektechnique.corefundamentals.challenge.service.CalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    //property based injection
//    @Autowired
//    private CalculationService calculationService;

    //setter based injection
//    private CalculationService calculationService;
//
//    @Autowired
//    public void setCalculationService(CalculationService calculationService) {
//        this.calculationService = calculationService;
//    }

    //constructor based injection
    private CalculationService calculationService;

    @Autowired
    public HomeController(CalculationService calculationService){
        this.calculationService = calculationService;
    }


    @Value("${msg.value1}")
    private String msg;

    @RequestMapping("/")
    public String getPageText()
    {
        //new line --> "\n" doesn't work when returning to request mapping
        //spaces auto truncate to one space as well
        return "from .yml: " + msg +
                "        ---       from CalcService:" + calculationService.thanks();
    }

}
