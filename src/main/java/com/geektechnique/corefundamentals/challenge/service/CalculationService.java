package com.geektechnique.corefundamentals.challenge.service;

import org.springframework.stereotype.Service;

@Service
public class CalculationService {

    public CalculationService(){}

    public int addStuff(int a, int b){
        return a + b;
    }

    public String thanks(){
        return "Thanks for using the calculation service!";
    }

}
